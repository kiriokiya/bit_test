<?php

namespace Src\Repositories;

use PDO;
use Src\Models\Wallet;

class WalletRepository extends BaseRepository
{
    protected function setTableName(): void
    {
        $this->tableName = Wallet::getTableName();
    }

    /**
     * @param int $id
     * @return Wallet
     * @throws \Exception
     */
    public function getWalletById(int $id): Wallet
    {
        $query = $this->db->prepare("SELECT * FROM {$this->tableName} WHERE id = :id");
        $query->execute(
            [
                ':id' => $id
            ]
        );

        $model = $query->fetchObject(Wallet::class);

        $this->checkExist($model);

        return $model;
    }

    /**
     * @param int $userId
     * @return Wallet[]
     */
    public function getWalletsByUserId(int $userId): array
    {
        $query = $this->db->prepare("SELECT * FROM {$this->tableName} WHERE user_id = :userId");
        $query->execute(
            [
                ':userId' => $userId
            ]
        );

        return $query->fetchAll(PDO::FETCH_CLASS, Wallet::class);
    }

    /**
     * @param int $walletId
     * @param int $userId
     * @return Wallet
     * @throws \Exception
     */
    public function getWalletByIdAndUserId(int $walletId, int $userId): Wallet
    {
        $query = $this->db->prepare("SELECT * FROM {$this->tableName} WHERE id = :id AND user_id = :userId");
        $query->execute([':id' => $walletId, ':userId' => $userId]);

        $model = $query->fetchObject(Wallet::class);

        $this->checkExist($model);

        return $model;
    }

    /**
     * @param int $walletId
     * @param float $sum
     * @return bool
     */
    public function updateBalance(int $walletId, float $sum): bool
    {
        $queryUpdate = $this->db->prepare(
            "UPDATE {$this->tableName} SET balance = balance - :sum WHERE id = :id"
        );
        return $queryUpdate->execute(
            [
                ':sum' => $sum,
                ':id' => $walletId
            ]
        );
    }

    /**
     * @param string $lockName
     */
    public function lockSelect(string $lockName): void
    {
        $stmt = $this->db->prepare("SELECT GET_LOCK(:lock, -1)");
        $stmt->execute([':lock' => $lockName]);
    }

    /**
     * @param string $lockName
     */
    public function unlockSelect(string $lockName): void
    {
        $stmt = $this->db->prepare("SELECT RELEASE_LOCK(:lock)");
        $stmt->execute([':lock' => $lockName]);
    }
}