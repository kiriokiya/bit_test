<?php

namespace Src\Repositories;

use Src\Models\Transaction;

class TransactionRepository extends BaseRepository
{
    protected function setTableName(): void
    {
        $this->tableName = Transaction::getTableName();
    }

    /**
     * @param int $walletId
     * @param float $sum
     * @return bool
     */
    public function createTransaction(int $walletId, float $sum): bool
    {
        $query = $this->db->prepare(
            "INSERT INTO {$this->tableName} (wallet_id, sum, date) VALUES (:walletId, :sum, :time)"
        );
        return $query->execute(
            [
                ':walletId' => $walletId,
                ':sum' => -$sum,
                ':time' => date('Y-m-d H:i:s')
            ]
        );
    }
}