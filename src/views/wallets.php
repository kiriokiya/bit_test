<?php
/** @var array $wallets */
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign Up</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>
<div class="wrapper">
    <h3>Кошельки пользователя <?php echo $this->user->getName(); ?></h3>
    <table class="table">
        <tr>
            <th scope="col">Номер</th>
            <th scope="col">Баланс</th>
            <th scope="col"></th>
        </tr>
        <?php
        /** @var \Src\Models\Wallet $wallet */
        foreach ($wallets as $wallet) {
            echo "<tr>
            <td>{$wallet->getNumber()}</td>
            <td>{$wallet->getBalance()}</td>" .
                '<td>
            <form action="/write-off" method="post">
                    <div class="form-group">
                        <label>Сумма</label>
                        <input type="number" step="0.01" name="sum" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="number" name="walletId" hidden class="form-control" ' .  "value=\"{$wallet->getId()}\">" .
                    '</div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Вывести">
                    </div>
                </form>
            
            </td>
            </tr>';
        }
        ?>
    </table>
</div>
</body>
</html>