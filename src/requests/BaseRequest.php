<?php

namespace Src\Requests;

abstract class BaseRequest
{
    /** @var array */
    protected $errors = [];

    public function validate()
    {
        $this->checkRules();

        if (!empty($this->errors)) {
            $message = implode(PHP_EOL, $this->errors);
            throw new \Exception($message);
        }
    }

    protected function baseStringCheck(string $field): void
    {
        if (!isset($this->$field) || !is_string($this->$field)) {
            $this->errors[] = "Неверное поле {$field}";
        }
    }

    protected function baseIntegerCheck(string $field): void
    {
        if (!isset($this->$field) || !is_int($this->$field)) {
            $this->errors[] = "Неверное поле {$field}";
        }
    }

    protected function baseFloatCheck(string $field): void
    {
        if (!isset($this->$field) || !is_numeric($this->$field)) {
            $this->errors[] = "Неверное поле {$field}";
        }
    }

    abstract protected function checkRules(): void;
}