<?php

namespace Src\Models;

abstract class Model
{
    abstract public static function getTableName(): string;

}