<?php

namespace Src\Services;

use Src\Connector;

abstract class BaseService
{
    /** @var \PDO */
    protected $db;

    public function __construct()
    {
        $this->db = Connector::get();
    }
}