<?php

namespace Src\Services;

use Src\Repositories\TransactionRepository;
use Src\Repositories\WalletRepository;

class WalletService extends BaseService
{
    /*** @var WalletRepository */
    public $walletRepository;
    /*** @var TransactionRepository */
    public $transactionRepository;

    public function __construct(WalletRepository $walletRepository, TransactionRepository $transactionRepository)
    {
        $this->walletRepository = $walletRepository;
        $this->transactionRepository = $transactionRepository;
        parent::__construct();
    }

    public function getWalletsByUserId(int $userId): array
    {
        return $this->walletRepository->getWalletsByUserId($userId);
    }

    /**
     * @param float $sum
     * @param int $walletId
     * @throws \Throwable
     */
    public function writeOff(float $sum, int $walletId): void
    {
        $this->db->beginTransaction();
        $lockName = "wallet{$walletId}";
        try {
            $this->walletRepository->lockSelect($lockName);
            $this->checkActualBalance($walletId, $sum);

            $updateResult = $this->walletRepository->updateBalance($walletId, $sum);
            $insertResult = $this->transactionRepository->createTransaction($walletId, $sum);
            $this->checkWriteOffResult($insertResult, $updateResult);

            $this->db->commit();

            $this->walletRepository->unlockSelect($lockName);
            $server = $_SERVER['HTTP_HOST'];
            header("Location: http://$server");
        } catch (\Throwable $exception) {
            $this->walletRepository->unlockSelect($lockName);
            $this->db->rollBack();
            throw $exception;
        }
    }

    /**
     * @param bool $insertResult
     * @param bool $updateResult
     * @throws \Exception
     */
    private function checkWriteOffResult(bool $insertResult, bool $updateResult): void
    {
        if (!$insertResult || !$updateResult) {
            throw new \Exception('Ошибка списания');
        }
    }

    /**
     * @param int $walletId
     * @param float $sum
     * @throws \Exception
     */
    private function checkActualBalance(int $walletId, float $sum): void
    {
        $wallet = $this->walletRepository->getWalletById($walletId);

        if ($wallet->getBalance() < $sum) {
            throw new \Exception('Недостаточно средств');
        }
    }
}